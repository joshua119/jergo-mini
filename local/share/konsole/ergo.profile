[Appearance]
ColorScheme=BlackOnWhite
Font=Droid Sans Mono Slashed,13,-1,5,50,0,0,0,0,0,Regular

[Cursor Options]
CursorShape=1

[General]
Name=ergo
Parent=FALLBACK/

[Interaction Options]
AutoCopySelectedText=true
TrimTrailingSpacesInSelectedText=true

[Scrolling]
HistoryMode=2

[Terminal Features]
BlinkingCursorEnabled=true
FlowControlEnabled=false
