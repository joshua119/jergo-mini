[Appearance]
ColorScheme=BlackOnRandomLight
Font=Droid Sans Mono Slashed,8,-1,5,50,0,0,0,0,0

[Cursor Options]
CursorShape=1

[General]
Name=remote
Parent=FALLBACK/

[Interaction Options]
AutoCopySelectedText=true
TrimTrailingSpacesInSelectedText=true

[Scrolling]
HistoryMode=2

[Terminal Features]
BlinkingCursorEnabled=true
FlowControlEnabled=false
