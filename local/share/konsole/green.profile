[Appearance]
ColorScheme=green
Font=Droid Sans Mono Slashed,8,-1,5,50,0,0,0,0,0

[Cursor Options]
CursorShape=1

[General]
LocalTabTitleFormat=%w
Name=green
Parent=FALLBACK/

[Scrolling]
HistoryMode=2

[Terminal Features]
BlinkingCursorEnabled=true
