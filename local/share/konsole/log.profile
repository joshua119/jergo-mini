[Appearance]
ColorScheme=Log
Font=Droid Sans Mono Slashed,6,-1,5,50,0,0,0,0,0

[Cursor Options]
CursorShape=1

[General]
Name=log
Parent=FALLBACK/

[Interaction Options]
AutoCopySelectedText=true

[Scrolling]
HistoryMode=2

[Terminal Features]
BlinkingCursorEnabled=true
FlowControlEnabled=false
