function proj_name {
    echo -n $(basename "$1" .sh) '';
}

export PROJECTS_HOME='/usr/local/share/projects.d'

# for cdargs
setup_cdargs 'p' "$PROJECTS_HOME";

alias personal='. ~/.ergo/proj.d/personal.sh';
alias consulting='. ~/.ergo/proj.d/consulting.sh';

# active projects
echo -n "proj: "
consulting
personal

echo ""
