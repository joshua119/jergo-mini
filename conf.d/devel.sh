alias catcert='openssl x509 -noout -text -in ';
alias byobu='byobu -q'

export LESSOPEN="| /usr/share/source-highlight/src-hilite-lesspipe.sh %s";
export LESS=' -R ';


function ttws
{
    # remove trailing whitespace
    sed -i 's/[ \t]*$//' "$1";

    # remove blank lines at end of file
    sed -i -e :a -e '/^\n*$/{$d;N;ba' -e '}' "$1";

    # ensure one newline char and end of file
    sed -i -e '$a\' "$1";
}

function fixbom
{
    # todo: fix this function. git diffs not working.

    # fix bom utf-8
    sed -i -e '1s/^\xef\xbb\xbf//' "$1";

    # fix bom utf-16
    sed -i -e '1s/^\xfe\xff//' "$1";

    # fix bom utf-16
    sed -i -e '1s/^\xff\xfe//' "$1";
}

function sdiff
{
    /usr/bin/sdiff "$1" "$2" | colordiff;
}


function yy2j
{
    python -c 'import sys, yaml, json; json.dump(yaml.load(sys.stdin), sys.stdout, indent=2)'
}

# function killd () {
#     for session in $(screen -ls | grep -o '[0-9]\{4\}')
#     do
#	screen -S "${session}" -X quit;
#     done
# }

. ~/.ergo/conf.d/python.sh
. ~/.ergo/conf.d/git.sh
. ~/.ergo/conf.d/docker.sh
