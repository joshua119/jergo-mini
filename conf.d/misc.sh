# For warning about QStandardPaths displayed by some Qt apps.
export XDG_RUNTIME_DIR="/tmp/runtime-${USER}";

export DPKG_PAGER='less -F -X -e';
