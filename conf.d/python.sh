#
# python
#
alias ipython='ipython --colors LightBG --no-banner --no-confirm-exit ';
#alias jupyter='jupyter console ';

# virtualenvwrapper
export WORKON_HOME='/usr/local/share/virtualenvs';

alias ipy='ipython --profile simple ';
