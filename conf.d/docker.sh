function de() {
    name="$1";
    docker exec -it "$name" bash;
}

function dps() {
    name="$1";
    docker ps -a | grep "$name";
}
